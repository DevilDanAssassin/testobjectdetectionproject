import json
from contextlib import asynccontextmanager

import cv2
import uvicorn
from fastapi import FastAPI, UploadFile, HTTPException
from starlette import status
from utils.check_file import check_file
from ultralytics import YOLO
import numpy as np

from utils.db_utils import get_db, insert_row, get_all

model = YOLO("yolov8n.pt")


@asynccontextmanager
async def lifespan(app: FastAPI):
    await get_db()
    yield


app = FastAPI(lifespan=lifespan)


@app.post("/image")
async def root(file: UploadFile):
    check_file(file)
    contents = await file.read()
    npimg = np.frombuffer(contents, np.uint8)
    img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
    results = model.predict(source=img)
    if len(results) > 0:
        response = json.loads(results[0].tojson())
        for i in response:
            await insert_row(file.filename, file.size, i["name"], results[0].orig_shape, i["box"])
        return response
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                        detail="Object not found")


@app.get("/all_images")
async def all_images():
    return await get_all()


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8001)

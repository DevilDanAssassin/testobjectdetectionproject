FROM python:3.11

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /app

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
RUN curl -sSL https://install.python-poetry.org | python - && \
    ln -s $HOME/.local/bin/poetry /usr/local/bin/poetry
COPY . /app

RUN poetry config virtualenvs.create false \
    && poetry install --no-dev
ENTRYPOINT python main.py


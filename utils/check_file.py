from fastapi import HTTPException, status, UploadFile


def check_file(file: UploadFile):
    SIZE = 10485760

    accepted_file_types = ["image/png", "image/jpeg", "image/jpg", "png","jpeg", "jpg"]

    if file.content_type not in accepted_file_types:
        raise HTTPException(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            detail="Unsupported file type",
        )

    if file.size > SIZE:
        raise HTTPException(status_code=status.HTTP_413_REQUEST_ENTITY_TOO_LARGE, detail="Too large")

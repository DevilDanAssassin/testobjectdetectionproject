import json
import os
from asyncpg import Connection
import asyncpg

conn: Connection | None = None


async def get_db():
    global conn
    conn = await asyncpg.connect(os.getenv("DATABASE_URL"))
    await conn.fetch(
        """create table if not exists file_object(
                name varchar(255),
                size float,
                shape jsonb,
                detection_name varchar(255),
                boxes jsonb);"""
    )


async def insert_row(name, size, detection_name, shape, boxes):
    global conn
    qry = """insert into file_object
           (name, size, detection_name, shape, boxes)
           values
           ($1, $2, $3, $4, $5)"""
    params = (name, size, detection_name, json.dumps(shape), json.dumps(boxes))
    await conn.execute(qry, *params)


async def get_all():
    qry = "select * from file_object"
    rows = await conn.fetch(qry)
    return rows

# Описание тестового задания

1. Запустить приложение с помощью команд        
	docker-compose build 
    docker-compose up -d  
      

2. Оптравить POST   
   localhost:8001/image  
   Нужно оправить форм-дату полем файл
   
     
3. для получения списка всех отправок    
   GET localhost:8001/all_images


4. Документация находится в ссылке   
   localhost:8001/docs
